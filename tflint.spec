%define debug_package %{nil}

Name:           tflint
Version:        0.54.0
Release:        1%{?dist}
Summary:        A Pluggable Terraform Linter

License:        MPLv2.0
URL:            https://github.com/terraform-linters/tflint
Source0:        https://github.com/terraform-linters/%{name}/releases/download/v%{version}/%{name}_linux_amd64.zip

%description
A Pluggable Terraform Linter

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
install -p -m 755 %{name} %{buildroot}/usr/bin

%files
/usr/bin/tflint

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Mon Oct 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version v0.53.0

* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version v0.51.1

* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v0.50.3

* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.50.0

* Wed Nov 15 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.49.0

* Mon Jun 19 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.47.0

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.46.1

* Thu Feb 16 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.45.0

* Sat Dec 31 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.44.1

* Fri Nov 25 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.43.0

* Tue Nov 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.42.2

* Sat Sep 24 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.41.0

* Sun Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Inital RPM
